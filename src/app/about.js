var React = require('react');
import{Router, Route, browserHistory, Link} from 'react-router'; 

require('./css/about.css'); 
var About = React.createClass({
	render: function(){
		return(
			<div>
			<div className="about">
			<nav className='nav'>
				<ul> 
					<li> <Link to={'/'}>Home &nbsp;&nbsp;</Link></li>
					<li> <Link to={'/about'}>About &nbsp;&nbsp;</Link></li>
					<li> <Link to={'/contact'}>Contact &nbsp;&nbsp;</Link></li>
				</ul>
			</nav>
			<h1> This Using was created by <a href="http://shanilplusplus.com" target="_blank">Shanil Sigera</a>  using React </h1>
			</div>

			</div>
		)
	}
});

module.exports = About;