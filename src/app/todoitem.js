var React = require('react');
// var ReactDOM = require('react-dom'); 

require('./css/todoitem.css');

var TodoItem  = React.createClass({
	render: function(){
		return(
			<li>
				<div className="todo-item">
					<span className="item-name">{this.props.todo} </span>
					<span className="item-delete" onClick={this.handleDelete}> X</span>
				</div>
			 </li>

		);
	},

	handleDelete: function(){
		this.props.onDelete(this.props.todo); 
	}
});


module.exports = TodoItem;