var React = require('react');
require('./css/addItem.css');

var AddItem = React.createClass({
	render: function(){
		return(
			<form id="add-todo" onSubmit={this.handleSubmit}>
				<input type="text" required ref="newItem"/>
				<button type="submit">Add</button>
			</form>
		);
	},

	handleSubmit: function(event){
		event.preventDefault();
		this.props.onAdd(this.refs.newItem.value);
		this.refs.newItem.value= '';
	}
})

module.exports = AddItem;