var React = require('react');
var ReactDOM = require('react-dom');
import{Router, Route, browserHistory, Link} from 'react-router'; 

require('./css/index.css'); 
var TodoItem = require('./todoitem');
var AddItem = require('./additem');
var About = require('./about');
var Contact = require('./contact');


var App = React.createClass({ 
	render: function(){
		return(
			<Router history={browserHistory}>
			<Route path={'/'} component={Todo} ></Route>
			<Route path={'/about'} component={About} ></Route>
			<Route path={'/contact'} component={Contact}> </Route>
			</Router>
		);
	}
})

var Todo = React.createClass({
	getInitialState: function(){
		return{
			todos: ['exaple todo 1', 'exaple todo 2', 'exaple todo 3']
		}
	},

	render: function(){
	var todos = this.state.todos;
	todos = todos.map(function(todo, index){
		return(
			<TodoItem todo={todo}  key={index} onDelete={this.onDelete}/>
		)
	}.bind(this));
	return(
		<div id="list">
			<nav className="nav">
				<ul> 
					<li> <Link to={'/'}>Home &nbsp;&nbsp;</Link></li>
					<li> <Link to={'/about'}>About &nbsp;&nbsp;</Link></li>
					<li> <Link to={'/about'}>Contact &nbsp;&nbsp;</Link></li>
				</ul>
			</nav>
			<h1>TO-DO LIST</h1>
			<h2> SEIZE THE DAY! </h2>
			<ul>
				<AddItem onAdd={this.onAdd}/>
				{todos}
			</ul>
		</div>
	);
	},
	onDelete: function(item){
		var updatedTodos =  this.state.todos.filter(function(val, index){
			return item !== val;
		});
		this.setState({
			todos: updatedTodos
		});
	},
	onAdd: function(item){
		var updatedTodos = this.state.todos;

		if(item.length > 0){
		updatedTodos.push(item);
		this.setState({
			todos: updatedTodos
		})
		}
		
		
	}
});

ReactDOM.render(<App />, document.getElementById('todo'));